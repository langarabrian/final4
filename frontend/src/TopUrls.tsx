import React, { useState, useEffect } from 'react';

import client from './feathers';
const urlsService = client.service('urls');

interface UrlInfo {
  _id: string,
  count: number,
  url: string
}

function TopUrls() {
  const [urls, setUrls] = useState([]);

  useEffect(() => {


  }, []);

  return(
    <div>
      <h3 className='mt-4'>Top 10 URLs Checked</h3>

      <table id='urls' className='table'>
        <thead>
          <tr>
            <th scope='col'>Count</th>
            <th scope='col'>URL</th>
          </tr>
        </thead>
        <tbody>


        </tbody>
      </table>

    </div>
  );
}

export default TopUrls;
