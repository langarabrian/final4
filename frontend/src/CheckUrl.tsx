import React, { useState, FormEvent } from 'react';

import client from './feathers';
const checkurlsService = client.service('checkurls');

function CheckUrl() {
  const [url, setUrl] = useState('');
  const [outboundLinks, setOutboundLinks] = useState([])

  function checkUrl( ev: FormEvent ) {
    const element = ev.currentTarget as HTMLFormElement;
    if ( element.checkValidity()) {
      console.log( 'URL: ' + url );




    } else {
      element.classList.add('was-validated');
    }
    ev.preventDefault();
  }

  return (
    <div>
      <h3 className='mt-4'>Check URL</h3>

      <div className='row'>
        <div className='col-md-12 order-md-1'>
          <form onSubmit={checkUrl} className='needs-validation' noValidate>

            <div className='mb-3'>
              <label htmlFor='url'>URL</label>
              <input
                type='url'
                className='form-control'
                id='url'
                placeholder=''
                value={url}
                required
                onChange={e => setUrl( e.target.value )} />
              <div className='invalid-feedback'>
                Valid URL is required.
              </div>
            </div>

            <button className='btn btn-primary' type='submit'>Check URL</button>

          </form>
        </div>
      </div>

      <h4 className='mt-4' id='outboundtitle'>Outbound Links</h4>

      <ul id='links' className='list-group'>



      </ul>
    </div>
  );
}

export default CheckUrl;
