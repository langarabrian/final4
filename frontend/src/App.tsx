import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import Home from './Home';
import CheckUrl from './CheckUrl';
import TopUrls from './TopUrls';

function App() {
  return (
    <Router>
      <div className='container'>
        <h1>Outbound Link Checker</h1>

        <div className='list-group'>
          <Link to='/check-url' className='list-group-item list-group-item-action'>Check URL</Link>
          <Link to='/top-urls' className='list-group-item list-group-item-action'>Top 10 URLs Checked</Link>
        </div>

        <Switch>
          <Route path='/check-url'>
            <CheckUrl />
          </Route>
          <Route path='/top-urls'>
            <TopUrls />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>

      </div>
    </Router>
  );
}

export default App;
