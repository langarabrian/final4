// Initializes the `checkurls` service on path `/checkurls`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Checkurls } from './checkurls.class';
import hooks from './checkurls.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'checkurls': Checkurls & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/checkurls', new Checkurls(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('checkurls');

  service.hooks(hooks);
}
