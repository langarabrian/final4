// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

import axios from 'axios';
import { Parser } from 'htmlparser2';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, result } = context;
    const urlsService = app.service( 'urls' );
    console.log( result.url );

    return context;
  };
};
