//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'checkurls\' service', () => {
  let mongod: any;
  let app: any;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    process.env.MONGODBURI = mongod.getUri();
    app = appFunc();
  });

  it('registered the service', () => {
    const service = app.service('checkurls');
    expect(service).toBeTruthy();
  });



});
