# final4

## Install and Build

These instructions are for the Google Cloud Shell environment starting in the project directory (final4).

NOTE: the first command is "." followed by a space followed by "install-mongod.sh"

```shell
$ . install-mongod.sh
$ cd frontend
$ yarn install
$ yarn build
$ cd ../backend
$ yarn install
```

## Run

Set your `PORT` and `MONGODBURI` environment variables.

From the `backend` directory:

```shell
$ yarn run dev
```
